(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-auto-show-menu t)
 '(ac-auto-start t)
 '(ac-use-fuzzy t)
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-enabled-themes (quote (smart-mode-line-dark)))
 '(custom-safe-themes
   (quote
    ("8885761700542f5d0ea63436874bf3f9e279211707d4b1ca9ed6f53522f21934" "c48551a5fb7b9fc019bf3f61ebf14cf7c9cdca79bcb2a4219195371c02268f11" "987b709680284a5858d5fe7e4e428463a20dfabe0a6f2a6146b3b8c7c529f08b" "e0d42a58c84161a0744ceab595370cbe290949968ab62273aed6212df0ea94b4" "e9776d12e4ccb722a2a732c6e80423331bcb93f02e089ba2a4b02e85de1cf00e" "58c6711a3b568437bab07a30385d34aacf64156cc5137ea20e799984f4227265" "3d5ef3d7ed58c9ad321f05360ad8a6b24585b9c49abcee67bdcbb0fe583a6950" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "0820d191ae80dcadc1802b3499f84c07a09803f2cb90b343678bdb03d225b26b" "4980e5ddaae985e4bae004280bd343721271ebb28f22b3e3b2427443e748cd3f" "28ec8ccf6190f6a73812df9bc91df54ce1d6132f18b4c8fcc85d45298569eb53" "71ecffba18621354a1be303687f33b84788e13f40141580fa81e7840752d31bf" "9b59e147dbbde5e638ea1cde5ec0a358d5f269d27bd2b893a0947c4a867e14c1" "3cd28471e80be3bd2657ca3f03fbb2884ab669662271794360866ab60b6cb6e6" "0c29db826418061b40564e3351194a3d4a125d182c6ee5178c237a7364f0ff12" "96998f6f11ef9f551b427b8853d947a7857ea5a578c75aa9c4e7c73fe04d10b4" "3cc2385c39257fed66238921602d8104d8fd6266ad88a006d0a4325336f5ee02" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "0c311fb22e6197daba9123f43da98f273d2bfaeeaeb653007ad1ee77f0003037" default)))
 '(display-battery-mode t)
 '(doc-view-continuous t)
 '(ecb-options-version "2.50")
 '(ediff-split-window-function (quote split-window-horizontally))
 '(ediff-window-setup-function (quote ediff-setup-windows-plain))
 '(eshell-mode-hook (quote (rename-eshell)))
 '(eshell-visual-commands
   (quote
    ("vi" "screen" "top" "less" "more" "lynx" "ncftp" "pine" "tin" "trn" "elm" "ssh")))
 '(eshell-visual-subcommands (quote (("vagrant" "ssh"))))
 '(fci-rule-color "#073642")
 '(focus-follows-mouse t)
 '(global-hl-line-mode t)
 '(grep-highlight-matches (quote always))
 '(grep-scroll-output t)
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#002b36" 0.25)
    (quote
     ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#93a1a1")
 '(highlight-tail-colors
   (quote
    (("#073642" . 0)
     ("#546E00" . 20)
     ("#00736F" . 30)
     ("#00629D" . 50)
     ("#7B6000" . 60)
     ("#8B2C02" . 70)
     ("#93115C" . 85)
     ("#073642" . 100))))
 '(hl-bg-colors
   (quote
    ("#7B6000" "#8B2C02" "#990A1B" "#93115C" "#3F4D91" "#00629D" "#00736F" "#546E00")))
 '(hl-fg-colors
   (quote
    ("#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36")))
 '(initial-major-mode (quote text-mode))
 '(initial-scratch-message nil)
 '(jira-url "https://jira.realitymine.com/")
 '(linum-format " %7i ")
 '(magit-diff-use-overlays nil)
 '(neo-window-fixed-size nil)
 '(nrepl-message-colors
   (quote
    ("#dc322f" "#cb4b16" "#b58900" "#546E00" "#B4C342" "#00629D" "#2aa198" "#d33682" "#6c71c4")))
 '(org-default-priority 73)
 '(org-enforce-todo-dependencies t)
 '(org-highest-priority 72)
 '(org-log-done nil)
 '(org-lowest-priority 76)
 '(org-startup-folded (quote showeverything))
 '(org-todo-keyword-faces
   (quote
    (("TODO" :foreground "red" :background dark)
     ("HOLD" :foreground "yellow" :background dark)
     ("DOING" :foreground "cyan" :background dark)
     ("NEXT" :foreground "HotPink" :background dark))))
 '(org-todo-keywords (quote ((sequence "TODO" "DOING" "HOLD" "NEXT" "DONE"))))
 '(package-selected-packages
   (quote
    (json-mode typescript-mode helm-ag ag yaml-mode ztree logview company-restclient restclient httprepl qml-mode git-timemachine vagrant chess psysh ecb markdown-mode groovy-mode magit lush-theme xwidgete noctilux-theme smart-mode-line which-key helm evil)))
 '(php-mode-coding-style (quote psr2))
 '(php-mode-speedbar-open nil)
 '(pos-tip-background-color "#073642")
 '(pos-tip-foreground-color "#93a1a1")
 '(projectile-completion-system (quote helm))
 '(save-place t nil (saveplace))
 '(scalable-fonts-allowed t)
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#073642" 0.2))
 '(term-default-bg-color "#002b36")
 '(term-default-fg-color "#839496")
 '(tool-bar-mode nil)
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#dc322f")
     (40 . "#c85d17")
     (60 . "#be730b")
     (80 . "#b58900")\
     (100 . "#a58e00")
     (120 . "#9d9100")
     (140 . "#959300")
     (160 . "#8d9600")
     (180 . "#859900")
     (200 . "#669b32")
     (220 . "#579d4c")
     (240 . "#489e65")
     (260 . "#399f7e")
     (280 . "#2aa198")
     (300 . "#2898af")
     (320 . "#2793ba")
     (340 . "#268fc6")
     (360 . "#268bd2"))))
 '(vc-annotate-very-old-color nil)
 '(web-mode-enable-auto-closing nil)
 '(weechat-color-list
   (quote
    (unspecified "#002b36" "#073642" "#990A1B" "#dc322f" "#546E00" "#859900" "#7B6000" "#b58900" "#00629D" "#268bd2" "#93115C" "#d33682" "#00736F" "#2aa198" "#839496" "#657b83")))
 '(xterm-color-names
   ["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#eee8d5"])
 '(xterm-color-names-bright
   ["#002b36" "#cb4b16" "#586e75" "#657b83" "#839496" "#6c71c4" "#93a1a1" "#fdf6e3"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#252525" :foreground "#cccccc" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 83 :width normal :foundry "CTDB" :family "Fira Mono"))))
 '(ediff-even-diff-A ((t (:background "SteelBlue4"))))
 '(ediff-even-diff-Ancestor ((t (:background "SteelBlue4"))))
 '(ediff-even-diff-B ((t (:background "SteelBlue4"))))
 '(ediff-even-diff-C ((t (:background "SteelBlue4"))))
 '(ediff-odd-diff-A ((t (:background "SteelBlue3"))))
 '(ediff-odd-diff-Ancestor ((t (:background "SteelBlue3"))))
 '(ediff-odd-diff-B ((t (:background "SteelBlue3"))))
 '(ediff-odd-diff-C ((t (:background "SteelBlue3"))))
 '(font-lock-comment-delimiter-face ((t (:foreground "SlateGray2"))))
 '(font-lock-comment-face ((t (:foreground "SlateGray4"))))
 '(font-lock-doc-face ((t (:foreground "SlateGray4"))))
 '(magit-section-highlight ((t (:background "color-235"))))
 '(mode-line ((t (:background "#292929" :foreground "#cccccc" :inverse-video nil :box (:line-width 1 :color "grey75") :underline nil :slant normal :weight normal))))
 '(mode-line-inactive ((t (:background "#202020" :foreground "#5f5f5f" :inverse-video nil :box (:line-width 1 :color "grey25") :underline nil :slant normal :weight normal))))
 '(org-level-1 ((t (:inherit outline-1))))
 '(org-level-2 ((t (:inherit outline-2 :foreground "LightGoldenrod"))))
 '(org-level-3 ((t (:inherit outline-3 :foreground "Cyan1"))))
 '(org-level-4 ((t (:inherit outline-4))))
 '(outline-1 ((t (:foreground "#aaccff" :inverse-video nil :underline nil :slant normal :weight bold :height 1.4))))
 '(outline-2 ((t (:foreground "#aadddd" :inverse-video nil :underline nil :slant normal :weight normal :height 1.25))))
 '(outline-3 ((t (:foreground "#aaeecc" :inverse-video nil :underline nil :slant normal :weight normal :height 1.2))))
 '(outline-4 ((t (:foreground "medium spring green" :inverse-video nil :underline nil :slant normal :weight normal :height 1.1))))
 '(php-function-call ((t (:foreground "plum")))))

 ;; STARTUP ==================================================

(menu-bar-mode 0)
(menu-bar-no-scroll-bar)

(setq frame-resize-pixelwise t)
(set-frame-position (selected-frame) 30 80)
(set-frame-size (selected-frame) 1024 600 t)


(setq split-width-threshold nil)
(setq inhibit-startup-screen t)


 ;; Change all propts to y or n

(setq confirm-kill-emacs 'y-or-n-p)
(fset 'yes-or-no-p 'y-or-n-p)

 ;; Mod KeySet

(global-set-key (kbd "C-k") 'kill-buffer)  ; Ctrl+k = Ctrl+c Ctrl+k
(global-set-key (kbd "M-k") 'kill-some-buffers) ; Meta+K
(global-set-key (kbd "C-c s f") 's3ed-find-file)
(global-set-key (kbd "C-c s s") 's3ed-save-file)

 ;; CUSTOM electric-pairs =============================================

(electric-pair-mode 1)
(setq electric-pair-pairs '(
        (?\< . ?\>)
        (?\{ . ?\})
        (?\` . ?\`)
    )
)


 ;; INSTALL PACKAGES =========================================

(require 'package)
(add-to-list
 'package-archives
 '("melpa-stable" . "https://melpa.org/packages/")
 t)
(package-initialize)
(when (not package-archive-contents)
    (package-refresh-contents))

(setq package-list
      '(
	evil
	helm
	which-key
	smart-mode-line
	noctilux-theme
	linum-relative
	php-eldoc
	p4
	company
	auto-complete
	ac-php
	key-chord
	flycheck
	flymake
	flymake-php
	flymake-phpcs
	puppet-mode
	ruby-mode
	web-mode
	magit
	ecb
	s3ed
	simple-httpd
	skewer-mode
	))
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))


 ;; REQUIRES AND MODES =======================================

(require 's3ed)
(require 'helm-config)
(evil-mode)
(s3ed-mode)
(which-key-mode t)
(key-chord-mode 1)
(linum-relative-global-mode t)
(autoload 'php-mode "php-mode" "Major mode for editing php code." t)
(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.inc$" . php-mode))
(setq-default recent-save-file "~/.emacs.d/recentf")
(column-number-mode 1)
(setq-default truncate-lines t)
(setq org-default-notes-file "~/org/organizer.org")
(setq org-agenda-files (list "~/org/work-agenda.org"))

 ;; APPEARANCE ===============================================

(load-theme 'noctilux)
(setq sml/no-confirm-load-theme t)
(sml/setup)
(setq neo-theme 'ascii)

(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

 ;; KEYBINDINGS ==============================================

(add-to-list 'display-buffer-alist '("*" display-buffer-same-window))
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(global-set-key (kbd "C-c C-z") 'ansi-term)
(setq hscroll-step 1)
(global-set-key (kbd "C-c m") 'magit-status)
(defun urxvt ()
  (interactive)
  (start-process "" nil "urxvt256c"))
(global-set-key [(control return)] 'urxvt)
(evil-set-initial-state 'ansi-term 'emacs)
(evil-set-initial-state 'eshell 'emacs)
(evil-set-initial-state 'git-timemachine-mode 'emacs)
(global-set-key (kbd "C-c C-g") 'helm-projectile-ag)

  ;; CUSTOM FUNCTIONS ========================================

(defun on-after-init ()
  (unless (display-graphic-p (selected-frame))
    (set-face-background 'default "unspecified-bg" (selected-frame))))

(setq frame-title-format
  '((buffer-file-name "%f"
    (dired-directory dired-directory "%b")) " - emacs"))

;; change mode-line color by evil state
(add-hook 'post-command-hook
	  (lambda ()
	    (let ((color (cond ((minibufferp) (cons (face-background 'mode-line) (face-background 'mode-line)))
			       ((evil-insert-state-p) '("#292989" . "#cccccc"))
			       ((evil-normal-state-p)  '("#292929" . "#cccccc"))
			       ((buffer-modified-p) '("#402929", "#cccccc"))
			       (t (cons (face-background 'mode-line) (face-background 'mode-line))))))
	      (set-face-background 'mode-line (car color))
	      )
	    (vc-refresh-state)))
